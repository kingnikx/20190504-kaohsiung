$(() => {
  const ARTIFACT_COLORS = [
    'blue', 'green', 'indigo', 'orange', 'pink' //, 'red'
  ];
  const ARTIFACT_NAMES = [
    '◯', '▽', '◇', '５', '６', 'Ｄ'
  ];
  const STORAGE_KEY_PREFIX = '20190504-Kaohsiung.';
  const STORAGE_KEY_PLAYER_NAME = STORAGE_KEY_PREFIX + 'name';
  const STORAGE_KEY_ARTIFACTS = STORAGE_KEY_PREFIX + 'artifacts';

  var $playernameModal= $('#playername-modal')
    .on('click', 'button', () => {
      var name = $('input[name=playnername]', $playernameModal).val().trim();
      if (name != '') {
        player.updateName(name);
        artifacts.download()
        $playernameModal.modal('hide');
      }
    })

  class Player {
    constructor() {
      this.updateName(localStorage.getItem(STORAGE_KEY_PLAYER_NAME) || '')
      if (this.name == '') {
        $playernameModal.modal('show')
      }
    }
    updateName(name) {
      $('input[name=playnername]').val(name)
      localStorage.setItem(STORAGE_KEY_PLAYER_NAME, name);
      this.name = name
    }
  }

  class Storage {
    constructor() {
      this.artifacts = JSON.parse(localStorage.getItem(STORAGE_KEY_ARTIFACTS) || '{}')
      this.download()
    }
    get(key) {
      return this.artifacts[key] || 0;
    }
    set(key, value) {
      this.artifacts[key] = value;
      localStorage.setItem(STORAGE_KEY_ARTIFACTS, JSON.stringify(this.artifacts));
    }
    each(func) {
      $('td.artifact').each(function() { func($(this).data('artifact')) });
      return this;
    }
    download() {
      if (player.name == '') return;

      $('button').prop('disabled', true);
      $.ajax({
        url: './artifacts/' + player.name,
        type: 'GET',
        contentType: 'application/json',
        success: data => {
          console.log(data);
          this.each(a => a.updateCount(data.artifacts[a.key] || 0))
          this.each(a => a.commit())
        },
        error: () => {
          alert('Server error');
        },
        complete: () => {
          $('button').prop('disabled', false);
        },
      })
    }
    upload() {
      this.each(a => a.commit())
      this.name = player.name
      if (this.name == '') return;

      $('button').prop('disabled', true);
      $.ajax({
        url: './artifacts',
        type: 'POST',
        data: JSON.stringify(this),
        dataType: 'json',
        contentType: 'application/json',
        success: data => {
          console.log(data);
        },
        error: () => {
          alert('Server error');
        },
        complete: () => {
          $('button').prop('disabled', false);
        },
      })
    }
    reset() {
      this.download()
    }
    clear() {
      this.each(a => a.clear())
    }
  }

  class Artifact {
    constructor(parent, color, name) {
      var self = this;
      this.key = color + '.' + name;
      this.$td = $('<td class=artifact>')
        .addClass(color)
        .append('<span class=name>' + name)
        .append('<br>')
        .append('(<span class=count></span>)')
        .data('artifact', this)
        .on('click', () => self.countUp())
        .appendTo(parent)
      this.reset()
    }
    updateCount(count) {
      if (count == 10) count = 0;
      $('.count', this.$td).text(this.count = Math.max(0, count))
    }
    countUp() {
      this.updateCount(this.count + 1)
    }
    clear() {
      this.updateCount(0)
    }
    reset() {
      this.updateCount(artifacts.get(this.key))
    }
    commit() {
      artifacts.set(this.key, this.count);
    }
  }

  var player = new Player();
  var artifacts = new Storage();

  var $table = $('#artifacts');
  ARTIFACT_COLORS.forEach(color => {
    var $tr = $('<tr>').appendTo($table);
    ARTIFACT_NAMES.forEach(name => new Artifact($tr, color, name));
  });

  $(document)
    .on('click', 'button[name=upload]', () => artifacts.upload())
    .on('click', 'button[name=reset]', () => artifacts.reset())
    .on('click', 'button[name=clear]', () => artifacts.clear());

  $(window).on('resize', () => {
    $('#artifacts').height($('body').height() - ($('header').height() + $('footer').height()) - 20);
    $('tr').css({
      fontSize: Math.min($('tr').height() / 3, $('tr').width() / 5)
    });
  }).trigger('resize')
})
