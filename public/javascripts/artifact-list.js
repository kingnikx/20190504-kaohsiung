$(() => {
  const ARTIFACT_COLORS = [
    'blue', 'green', 'indigo', 'orange', 'pink' //, 'red'
  ];
  const ARTIFACT_NAMES = [
    '◯', '▽', '◇', '５', '６', 'Ｄ'
  ];

  var $table = $('#artifacts');
  (() => {
    var $thead = $('<thead>').appendTo($table);
    var $tr = $('<tr>').appendTo($thead);
    $('<th class=name>name</td>').appendTo($tr);
    ARTIFACT_COLORS.forEach(color => {
      ARTIFACT_NAMES.forEach(name => {
        $('<th>')
          .addClass(color)
          .text(name)
          .appendTo($tr);
      });
    });
  })();

  var $tbody = $('<tbody>').appendTo($table);
  var listArtifacts = data => {
    $tbody.empty()
    Object.keys(data).sort().forEach(name => {
      var $tr = $('<tr>').appendTo($tbody);
      $('<td class=name>')
        .text(name)
        .appendTo($tr);

      var artifacts = data[name];
      ARTIFACT_COLORS.forEach(color => {
        ARTIFACT_NAMES.forEach(name => {
          var key = color + '.' + name;
          $('<td class=artifact>')
            .addClass(color)
            .text(artifacts[key] || 0)
            .appendTo($tr);
        });
      });
    });
  }

  $.ajax({
    url: './artifacts',
    type: 'GET',
    dataType: 'json',
    contentType: 'application/json',
    success: data => {
      listArtifacts(data)
    },
    error: () => {
      alert('Server error');
    }
  })
})
