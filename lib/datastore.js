const Redis = require('ioredis');
const redis = new Redis()

const fs = require('fs');
const DATA_FILE = './artifacts.json'

const REDIS_KEY = 'artifacts'

class DataStore {
  constructor() {
    try {
      var content = fs.readFileSync(DATA_FILE);
      var data = JSON.parse(content);
      for (var key in data) {
        redis.hsetnx(REDIS_KEY, key, JSON.stringify(data[key]));
      }
    }
    catch(e) {
      // ignore exception
    }
  }
  getAll(callback) {
    redis.hgetall(REDIS_KEY, (error, result) => {
      var data = {}
      for (var key in result || {}) {
        data[key] = JSON.parse(result[key]);
      }
      callback(data);
    });
  }
  get(key, callback) {
    redis.hget(REDIS_KEY, key, (error, result) => {
      callback(JSON.parse(result || '{}'));
    })
  }
  put(key, value) {
    redis.hset(REDIS_KEY, key, JSON.stringify(value))
  }
  flush() {
    this.getAll(data => {
      fs.writeFileSync(DATA_FILE, JSON.stringify(data, null, 2));
      redis.del(REDIS_KEY);
    })
  }
}

module.exports = () => new DataStore();
