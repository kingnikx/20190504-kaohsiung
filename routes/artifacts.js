var express = require('express');
var router = express.Router();

var datastore = require('../lib/datastore')()


router.get('', function(req, res, next) {
  if (req.headers['content-type'] == 'application/json') {
    datastore.getAll(data => res.json(data));
  }
  else {
    res.render('artifacts');
  }
});

router.get('/:name', function(req, res, next) {
  var name = req.params.name;
  datastore.get(name, value => {
    res.json({
      name: name,
      artifacts: value || {}
    });
    })
});

router.post('/', function(req, res, next) {
  var params = req.body;
  datastore.put(params.name, params.artifacts);
  res.json({status: 0});
});

module.exports = router;
